pipeline {
  agent any


  environment {
    PROJECT_NAME='Deploy Application'
    PROJECT='demo'
    DOMAIN='craft.adigheorghe.ro'
    STACK='democraft'
    DOCKER_REGISTRY='https://repo.treescale.com'
    CONTAINER='adighe/democraft'
    VERSION="1.${BUILD_NUMBER}"
    ARTEFACT="1.${BUILD_NUMBER}.tgz"
    WORKSPACE = pwd()
  }
  stages {
    stage('Create Artefact') {
      steps {
        sshagent (['jenkins']) {
            script {
                sh '''
                    echo ${ARTEFACT}
                    echo ${VERSION}
                    git tag -a "v${VERSION}" -m "Jenkins"
                    git push origin "v${VERSION}" -vvv
                    cd "${WORKSPACE}"
                    cp docker/php/debian/* .
                '''
            }
        }
      }
    }
    stage('Build Image') {
      steps {
        script {
            docker.withRegistry("${DOCKER_REGISTRY}", 'docker-registry-treescale') {
                def img = docker.build("${CONTAINER}:${VERSION}", "--build-arg PHP_VERSION=7.1 .")
                img.push()
                sh "docker rmi ${img.id}"
            }
        }
      }
    }
    stage('Deploy Stack') {
      steps {
          withCredentials([
              usernamePassword(
                  credentialsId: 'docker-registry-treescale',
                  usernameVariable: 'DOCKER_USER',
                  passwordVariable: 'DOCKER_PASSWORD'
              )
          ])
          {
            script {
                echo "Deploying Container Stack to Docker Cluster"
                sh "ansible-playbook -i devops/inventories/manager1/hosts devops/manager1.yml --extra-vars=\"{'WORKSPACE': '${env.WORKSPACE}', 'DOMAIN': '${env.DOMAIN}', 'PROJECT': '${env.PROJECT}', 'STACK': '${env.STACK}', 'VERSION': '${env.VERSION}', 'DOCKER_REGISTRY': '${env.DOCKER_REGISTRY}', 'DOCKER_USER': '${env.DOCKER_USER}', 'DOCKER_PASSWORD': '${env.DOCKER_PASSWORD}', 'STACK_DB_PASSWORD': '${env.STACK_DB_PASSWORD}'}\" -vvv"
            }
          }
      }
    }
  }
}
