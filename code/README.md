### CraftCms Docker development environment

### Structure
- One MySQL 5.7 container. The version can be changed at any time. Local volume set to /var/lib/mysql for database persistence
- One PHP container. The PHP image has build arguments in order to customize the PHP version, Drupal version
- One Nginx container.
- One PHP Install container. This container waits for the database to be ready and runs the installation with default values. Also uses build arguments.

### URLs
- http://halo-craftcms.localhost/
- http://adminer.halo-craftcms.localhost/
- http://halo-craftcms.localhost/admin/


### Install
- Clone repository where you want to set up a craftcms installation
- Change services php and install build arguments to suit your needs
- Raise containers using docker-compose up -d or docker-compose up -d --build

### Usage
#### Craft CLI
docker exec -ti craftphp ./craft

#### Craft Clear Cache
docker exec -ti craftphp ./craft cache/flush-all

#### Composer
docker exec -ti craftphp composer install vendor/package --prefer-dist